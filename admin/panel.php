<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../public/styles/style_panel.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <header id="navbar">
                <?php
                session_start();
                include '../funcs/connect.php';

                    if($_SESSION['admin-login']==1){
                        if(isset($_GET['user'])){
                            $query = "SELECT * FROM admin WHERE username=?";
                            $result = $connect->prepare($query);
                            $result->bindValue(1, $_GET['user']);
                            $result->execute();

                            while ($rows=$result->fetch(PDO::FETCH_ASSOC)){

                                echo "<div><a  href='logout.php' onclick='Exit()'><img id='logout-img' src=../admin/pic/inside-logout-icon.png></a></div>
                                      <div id='admin-name'>".$rows["fname"]."  ".$rows["lname"]."</div>";


                            }
                        }

                    }else{
                        header('location:login.php');
                        exit;
                    }
                ?>
        </header>
        <div class="main">

            <div id="center"> <iframe id="iframe" name="frame" ></iframe> </div>
            <div id="rmenu">
                <ul id="ul-r">
                    <li><a href="category/cat-manage.php" target="frame">مدیریت دسته ها</a></li>
                    <li><a href="">مدیریت محصولات </a></li>
                    <li><a href="">مدیریت سفارشات </a> </li>
                    <li> <a href="">مدیریت کاربران</a></li>
                    <li><a href="">مدیریت نظرات</a> </li>
                </ul>
            </div>


        </div>
    </div>
<script>
    function Exit() {
        let x = confirm("آیا مایل به خروج هستید؟")
    }
</script>
</body>
</html>