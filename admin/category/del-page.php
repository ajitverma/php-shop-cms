<?php
include "../../funcs/connect.php";
if(isset($_GET['id'])) {
    $sql = 'DELETE FROM `cat` WHERE `cat`.`catid` =? ';
    $result = $connect->prepare($sql);
    $result->bindValue(1, $_GET['id']);
    $query = $result->execute();
    if($query){
        header('location:cat-manage.php?del=1010');
        exit;
    }else{
        header('location:cat-manage.php?err=2020');
        exit;
    }
}else{
    header('location:cat-manage.php');
    exit;
}