<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../public/styles/style_login.css">
    <style>
        @font-face {
            font-family: Vazir;
            src: url("../public/Vazir.ttf");
        }
        *{
            font-family: Vazir;
        }
    </style>
</head>
<body>
    <div class="container">
            <picture id="admin-img">
                <img src="pic/head-admin-icon-ts3-13.jpg.png">
            </picture>
            <div id="php-tag">
                <?php
                session_start();
                include "../funcs/connect.php";
                if(isset($_POST['btn'])){
                    if (empty($_POST['username']) || empty($_POST['password'])){
                        echo 'کادر ها خالی می باشند';
                        echo '<style> #username, #password{box-shadow: 1px 1px 2px 3px rgba(255, 51, 51, .2)} </style>';
                    }else {
                        $sql = "SELECT * FROM admin WHERE  username=? && password=?";
                        $result = $connect->prepare($sql);
                        $result->bindValue(1, $_POST['username']);
                        $result->bindValue(2, $_POST['password']);
                        $result->execute();
                        $num = $result->fetchColumn();
                        if ($num == 1){
                            header("location:panel.php?user=$_POST[username]");
                            $_SESSION["admin-login"] = 1;
                            exit;

                        }else{
                            echo 'خطایی رخ داده است';
                            echo '<style> #username, #password{box-shadow: 1px 1px 2px 2px rgba(230, 230, 0, .2)} </style>';
                        }
                    }
                }

                ?>
            </div >
            <form method="post" id="form-ctl">
                <input type="text" name="username" id="username" placeholder="username">
                <input type="password" name="password"  id="password" placeholder="password">
                <input type="submit" name="btn" value="Login" id="login">
                <a id="Forget" href="#">Forget Username or Password?</a>
            </form>
    </div>



    <script src="../public/bootstrap/bootstrap.min.js"></script>
</body>
</html>