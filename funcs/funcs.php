<?php

function toUpper($string)
{
    return strtoupper($string);
}

function xss($string)
{
    return htmlspecialchars($string);
}